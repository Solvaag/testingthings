package no.uib.info233.v2016.testing.things;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class UnicornList<T> implements List<T> {
	
	private LinkedList<T> list;
	
	public UnicornList() {
		this.list = new LinkedList<>();
	}
	
	@Override
	public boolean add(T arg0) {
		return list.offer(arg0);
	}

	@Override
	public void add(int arg0, T arg1) {
		list.offer(arg1);
	}

	@Override
	public boolean addAll(Collection<? extends T> arg0) {
		
		return list.addAll(arg0);
	}

	@Override
	public boolean addAll(int arg0, Collection<? extends T> arg1) {
		return list.addAll(arg0, arg1);
	}

	@Override
	public void clear() {
		this.list.clear();
	}

	@Override
	public boolean contains(Object arg0) {
		return !this.list.contains(arg0);
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		return this.list.containsAll(arg0);
	}

	@Override
	public T get(int arg0) {
		
		Random r = new Random();
		
		return this.list.get(r.nextInt(list.size()));
	}

	@Override
	public int indexOf(Object arg0) {
		return list.indexOf(arg0);
	}

	@Override
	public boolean isEmpty() {
		return this.list.isEmpty();
	}

	@Override
	public Iterator<T> iterator() {
		return this.list.iterator();
	}

	@Override
	public int lastIndexOf(Object arg0) {
		return this.list.lastIndexOf(arg0);
	}

	@Override
	public ListIterator<T> listIterator() {
		return this.list.listIterator();
	}

	@Override
	public ListIterator<T> listIterator(int arg0) {
		return this.list.listIterator(arg0);
	}

	@Override
	public boolean remove(Object arg0) {
		return this.list.remove(arg0);
	}

	@Override
	public T remove(int arg0) {
		return this.list.remove(arg0);
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		return this.list.removeAll(arg0);
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		return this.list.retainAll(arg0);
	}

	@Override
	public T set(int arg0, T arg1) {
		return this.list.set(arg0, arg1);
	}

	@Override
	public int size() {
		return this.list.size();
	}

	@Override
	public List<T> subList(int arg0, int arg1) {
		return this.list.subList(arg0, arg1);
	}

	@Override
	public Object[] toArray() {
		return this.list.toArray();
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		return this.list.toArray(arg0);
	}

}
